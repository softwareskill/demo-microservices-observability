# Observability dev stack

### Services

| Service | URL | Category |
| --- | --- | --- |
| Splunk | http://localhost:8000 | Log aggregation |
| Consul | http://localhost:8500 | Service discovery |
| Grafana | http://localhost:3000 | Monitoring (dashboard) |
| Prometheus | http://localhost:9090 | Monitoring (datasource) |
| Zipkin | http://localhost:9411 | Tracing |

### Stack setup

1. Uruchom stos monitoringu
   ```
   docker-compose up -d
   ```
2. Otwórz grafanę http://localhost:3000/
   - Login: admin
   - Hasło: admin
3. Dodaj Prometherus'a jako źródło danych:
   - Configuration -> Data Sources -> Add Data Source
   - Url: http://host.docker.internal:9090
