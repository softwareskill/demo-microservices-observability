package pl.softwareskill.sample.microservices.orders.domain.model;

import lombok.Getter;
import lombok.ToString;

import java.util.UUID;

@Getter
@ToString
public class Order {

    private UUID id;
    private String description;

    private Order(UUID id, String description) {
        this.id = id;
        this.description = description;
    }

    public static Order create(UUID id, String description) {
        return new Order(id, description);
    }
}
