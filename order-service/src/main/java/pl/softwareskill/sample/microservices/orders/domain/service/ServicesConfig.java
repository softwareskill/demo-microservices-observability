package pl.softwareskill.sample.microservices.orders.domain.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import pl.softwareskill.sample.microservices.orders.domain.model.OrderProcessor;
import pl.softwareskill.sample.microservices.orders.domain.model.OrderRepository;
import pl.softwareskill.sample.microservices.orders.domain.service.processor.LoyalityProgramProcessor;
import pl.softwareskill.sample.microservices.orders.domain.service.processor.NotificationProcessor;
import pl.softwareskill.sample.microservices.orders.domain.service.processor.ShippingProcessor;
import reactor.core.scheduler.Schedulers;

import java.util.Collection;

@Configuration
class ServicesConfig {

    @Bean
    public CreateOrderService createOrderService(OrderRepository orderRepository, ProcessOrderService processOrderService) {
        return new CreateOrderService(orderRepository, processOrderService);
    }

    @Bean
    public ProcessOrderService processOrderService(Collection<OrderProcessor> orderProcessors) {
        var scheduler = Schedulers.newElastic("order-processing");
        return new ProcessOrderService(scheduler, orderProcessors);
    }

    @Bean
    @Order(1)
    public ShippingProcessor shippingProcessor() {
        return new ShippingProcessor();
    }

    @Bean
    @Order(2)
    public NotificationProcessor notificationProcessor() {
        return new NotificationProcessor();
    }

    @Bean
    @Order(3)
    public LoyalityProgramProcessor loyalityProgramProcessor() {
        return new LoyalityProgramProcessor();
    }
}
