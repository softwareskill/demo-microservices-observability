package pl.softwareskill.sample.microservices.orders.infra;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.softwareskill.sample.microservices.orders.domain.model.OrderRepository;
import pl.softwareskill.sample.microservices.orders.infra.InMemoryOrderRepository;

@Configuration
public class OrderRepositoryConfig {

    @Bean
    public OrderRepository orderRepository() {
        return new InMemoryOrderRepository();
    }
}
