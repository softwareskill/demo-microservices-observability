package pl.softwareskill.sample.microservices.orders.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.sample.microservices.orders.domain.service.CreateOrderService;
import pl.softwareskill.sample.microservices.orders.domain.model.Order;
import pl.softwareskill.sample.microservices.orders.rest.dto.CreateOrderDto;

import static java.util.Collections.singletonMap;

@Slf4j
@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
public class CreateOrderController {

    private final CreateOrderService createOrderService;

    @PostMapping
    public ResponseEntity create(@RequestBody CreateOrderDto createOrder) {
        log.info("Creating order: {}", createOrder);
        Order createdOrder = createOrderService.createOrder(createOrder.getDescription());
        log.info("Created order: {}", createdOrder);
        return createResponse(createdOrder);
    }

    private ResponseEntity createResponse(Order createdOrder) {
        return ResponseEntity
                .accepted()
                .body(singletonMap("orderId", createdOrder.getId().toString()));
    }
}
