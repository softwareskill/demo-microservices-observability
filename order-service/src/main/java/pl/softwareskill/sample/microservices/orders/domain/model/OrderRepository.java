package pl.softwareskill.sample.microservices.orders.domain.model;

import java.util.Optional;
import java.util.UUID;

public interface OrderRepository {

    Optional<Order> find(UUID orderId);

    void save(Order order);

    void remove(UUID orderId);
}
