package pl.softwareskill.sample.microservices.orders.rest.dto;

import lombok.Data;

@Data
public class CreateOrderDto {
    String description;
}
