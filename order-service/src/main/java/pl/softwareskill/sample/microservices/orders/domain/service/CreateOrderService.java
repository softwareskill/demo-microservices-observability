package pl.softwareskill.sample.microservices.orders.domain.service;

import lombok.RequiredArgsConstructor;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import pl.softwareskill.sample.microservices.orders.domain.model.Order;
import pl.softwareskill.sample.microservices.orders.domain.model.OrderRepository;

import java.util.UUID;

@RequiredArgsConstructor
public class CreateOrderService {

    private final OrderRepository orderRepository;
    private final ProcessOrderService processOrderService;

    @NewSpan
    public Order createOrder(String description) {
        UUID orderId = UUID.randomUUID();
        Order createdOrder = Order.create(orderId, description);

        orderRepository.save(createdOrder);

        processOrderService.processOrder(createdOrder);

        return createdOrder;
    }
}
