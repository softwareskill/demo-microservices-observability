package pl.softwareskill.sample.microservices.orders.domain.model;

@FunctionalInterface
public interface OrderProcessor {

    void process(Order order);
}
