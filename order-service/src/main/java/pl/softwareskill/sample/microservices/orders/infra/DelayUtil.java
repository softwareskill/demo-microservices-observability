package pl.softwareskill.sample.microservices.orders.infra;

import java.time.Duration;

public class DelayUtil {

    public static void delay(Duration duration) {
        try {
            Thread.sleep(duration.toMillis());
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
