package pl.softwareskill.sample.microservices.orders.domain.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import pl.softwareskill.sample.microservices.orders.domain.model.Order;
import pl.softwareskill.sample.microservices.orders.domain.model.OrderProcessor;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;

import java.util.ArrayList;
import java.util.Collection;

@Slf4j
class ProcessOrderService {

    private final Scheduler scheduler;
    private final Collection<OrderProcessor> orderProcessors;

    public ProcessOrderService(Scheduler scheduler, Collection<OrderProcessor> orderProcessors) {
        this.scheduler = scheduler;
        this.orderProcessors = new ArrayList<>(orderProcessors);
    }

    @NewSpan("process order")
    public void processOrder(Order order) {
        Flux.fromIterable(orderProcessors)
                .doOnNext(processor -> processor.process(order))
                .onErrorContinue(this::onError)
                .subscribeOn(scheduler)
                .subscribe();
    }

    private void onError(Throwable e, Object causedBy) {
        log.error("Error during processing event {}", causedBy, e);
    }
}
