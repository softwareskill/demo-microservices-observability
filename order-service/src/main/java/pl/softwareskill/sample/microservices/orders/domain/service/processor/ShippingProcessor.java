package pl.softwareskill.sample.microservices.orders.domain.service.processor;

import org.springframework.cloud.sleuth.annotation.NewSpan;
import pl.softwareskill.sample.microservices.orders.domain.model.Order;
import pl.softwareskill.sample.microservices.orders.domain.model.OrderProcessor;
import pl.softwareskill.sample.microservices.orders.infra.DelayUtil;

import java.time.Duration;

public class ShippingProcessor implements OrderProcessor {

    @Override
    @NewSpan("shipping")
    public void process(Order order) {
        DelayUtil.delay(Duration.ofSeconds(1));
    }
}
