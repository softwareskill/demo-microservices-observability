package pl.softwareskill.sample.microservices.orders.domain.service.processor;

import org.springframework.cloud.sleuth.annotation.NewSpan;
import pl.softwareskill.sample.microservices.orders.domain.model.Order;
import pl.softwareskill.sample.microservices.orders.domain.model.OrderProcessor;
import pl.softwareskill.sample.microservices.orders.infra.DelayUtil;

import java.time.Duration;

public class NotificationProcessor implements OrderProcessor {

    @Override
    @NewSpan("notification")
    public void process(Order order) {
        DelayUtil.delay(Duration.ofSeconds(3));
        throw new IllegalStateException("Error during sending notification.");
    }
}
