package pl.softwareskill.sample.microservices.basket.infra;

import io.micrometer.core.instrument.MeterRegistry;
import lombok.SneakyThrows;
import org.infinispan.commons.marshall.JavaSerializationMarshaller;
import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfiguration;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.softwareskill.sample.microservices.basket.domain.model.Basket;
import pl.softwareskill.sample.microservices.basket.domain.model.BasketItem;
import pl.softwareskill.sample.microservices.basket.domain.model.BasketRepository;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

@Configuration
class BasketRepositoryConfig {

    @Bean
    @ConditionalOnProperty(value = "basket.repository.monitoring.enabled", havingValue = "false", matchIfMissing = false)
    BasketRepository basketRepository() {
        return new InfinispanBasketRepository(basketsCache());
    }

    @Bean(initMethod = "start")
    @ConditionalOnProperty(value = "basket.repository.monitoring.enabled", havingValue = "true", matchIfMissing = true)
    BasketRepository monitoredBasketRepository(MeterRegistry meterRegistry) {
        return new MonitoredInfinispanBasketRepository(basketsCache(), meterRegistry);
    }

    @Bean
    @SneakyThrows
    Map<UUID, Basket> basketsCache() {
        ConfigurationBuilder cb = new ConfigurationBuilder();

        cb.clustering().cacheMode(CacheMode.REPL_SYNC);
        cb.statistics().enable();

        org.infinispan.configuration.cache.Configuration c = cb.build();
        EmbeddedCacheManager manager = new DefaultCacheManager(globalConfig());
        manager.defineConfiguration("replicatedBasketCache", c);
        return manager.getCache("replicatedBasketCache");
    }

    private GlobalConfiguration globalConfig() {
        GlobalConfigurationBuilder globalConfig = new GlobalConfigurationBuilder();

        globalConfig
                .cacheContainer()
                .statistics(true)
                .jmx()
                .enable();

        globalConfig
                .transport()
                .defaultTransport()
                .clusterName("qa-cluster")
                .addProperty("configurationFile", "default-configs/default-jgroups-udp.xml");

        globalConfig
                .serialization()
                .marshaller(new JavaSerializationMarshaller())
                .whiteList()
                .addClasses(Basket.class, BasketItem.class, UUID.class, ArrayList.class);

        return globalConfig.build();
    }
}
