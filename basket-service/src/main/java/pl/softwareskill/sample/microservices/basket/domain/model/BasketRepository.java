package pl.softwareskill.sample.microservices.basket.domain.model;

import java.util.Optional;
import java.util.UUID;

public interface BasketRepository {

    Optional<Basket> findById(UUID basketId);

    void save(Basket basket);

    void remove(Basket basket);
}
