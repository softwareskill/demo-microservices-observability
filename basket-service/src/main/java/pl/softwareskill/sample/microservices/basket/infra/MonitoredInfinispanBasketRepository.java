package pl.softwareskill.sample.microservices.basket.infra;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import pl.softwareskill.sample.microservices.basket.domain.model.Basket;
import pl.softwareskill.sample.microservices.basket.domain.model.BasketRepository;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
class MonitoredInfinispanBasketRepository implements BasketRepository {

    private final Map<UUID, Basket> store;
    private final MeterRegistry meterRegistry;
    private Counter counter;

    public void start() {
        counter = meterRegistry.counter("baskets.counter");
        meterRegistry.gauge("baskets.gauge", store, Map::size);
    }

    @NewSpan
    @Override
    public Optional<Basket> findById(@SpanTag("basketId") UUID basketId) {
        return Optional.ofNullable(store.get(basketId));
    }

    @NewSpan
    @Override
    public void save(Basket basket) {
        store.put(basket.getBasketId(), basket);
        counter.increment();
    }

    @NewSpan
    @Override
    public void remove(Basket basket) {
        store.remove(basket.getBasketId(), basket);
    }
}
