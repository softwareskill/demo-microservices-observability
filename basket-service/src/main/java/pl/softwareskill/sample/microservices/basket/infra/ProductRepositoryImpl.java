package pl.softwareskill.sample.microservices.basket.infra;

import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import pl.softwareskill.sample.microservices.basket.domain.model.Product;
import pl.softwareskill.sample.microservices.basket.domain.model.ProductRepository;

import java.time.Duration;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

import static java.lang.Boolean.parseBoolean;

@RequiredArgsConstructor
class ProductRepositoryImpl implements ProductRepository {

    private final Collection<Product> products;
    private final boolean doDelay = parseBoolean(System.getProperty("product.repo.delay.enabled", "false"));

    @Override
    @NewSpan
    @Timed("product_repository_get_by_id")
    public Optional<Product> getProductById(@SpanTag("productId") String productId) {
        doDelay();

        return products
                .stream()
                .filter(p -> Objects.equals(productId, p.getProductId()))
                .findFirst();
    }

    @SneakyThrows
    private void doDelay() {
        if (doDelay) {
            var delay = Duration.ofMillis(100 + RandomUtils.nextInt(100));
            DelayUtil.delay(delay);
        }
    }
}
