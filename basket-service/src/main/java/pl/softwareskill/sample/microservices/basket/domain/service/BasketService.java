package pl.softwareskill.sample.microservices.basket.domain.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import pl.softwareskill.sample.microservices.basket.domain.model.Basket;
import pl.softwareskill.sample.microservices.basket.domain.model.BasketRepository;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
public class BasketService {

    private final BasketRepository basketRepository;

    @NewSpan("Create Basket")
    public Basket createBasket() {
        Basket basket = Basket.create();
        basketRepository.save(basket);

        log.info("Basket created: {}", basket.getBasketId());

        return basket;
    }

    @NewSpan("Remove Basket")
    public void removeBasket(UUID basketId) {
        Basket basket = getBasket(basketId);
        basketRepository.remove(basket);

        log.info("Basket removed: {}", basket.getBasketId());
    }

    private Basket getBasket(UUID basketId) {
        return basketRepository.findById(basketId)
                .orElseThrow(() -> new NotFoundException("Basket not found."));
    }
}
