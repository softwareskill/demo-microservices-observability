package pl.softwareskill.sample.microservices.basket.infra;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;
import pl.softwareskill.sample.microservices.basket.domain.service.OrderService;

import java.time.Duration;

@Configuration
class OrderServiceConfig {

    @Bean
    OrderService orderService(WebClient orderServiceWebClient,
                              @Value("${order.service.timeout}") String timeout) {
        return new OrderService(orderServiceWebClient, Duration.parse(timeout));
    }

    @Bean
    WebClient orderServiceWebClient(@Value("${order.service.url}") String url) {
        return WebClient.builder()
                .baseUrl(url)
                .build();
    }
}
