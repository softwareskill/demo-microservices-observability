package pl.softwareskill.sample.microservices.basket.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.sample.microservices.basket.domain.service.OrderService;

import java.util.HashMap;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
class OrderController {

    private final OrderService orderService;

    @PostMapping("/basket/{basketId}/order")
    public ResponseEntity<?> createOrder(@PathVariable UUID basketId) {
        log.info("Create order from basket {}", basketId);
        var orderId = orderService.createOrder("aa");
        return orderCreatedResponse(orderId);
    }

    private ResponseEntity<?> orderCreatedResponse(UUID orderId) {
        var body = new HashMap<>();
        body.put("orderId", orderId);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(body);
    }
}
