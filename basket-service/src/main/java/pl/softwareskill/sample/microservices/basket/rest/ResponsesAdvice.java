package pl.softwareskill.sample.microservices.basket.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.softwareskill.sample.microservices.basket.domain.service.NotFoundException;

import java.util.HashMap;

@ControllerAdvice
class ResponsesAdvice {

    @ExceptionHandler(value = {NotFoundException.class })
    public ResponseEntity<?> resourceNotFoundException(Exception exception) {
        var body = new HashMap<>();
        body.put("message", exception.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(body);
    }
}
