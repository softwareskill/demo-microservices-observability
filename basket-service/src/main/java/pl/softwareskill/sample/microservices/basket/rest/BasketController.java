package pl.softwareskill.sample.microservices.basket.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.softwareskill.sample.microservices.basket.domain.model.Basket;
import pl.softwareskill.sample.microservices.basket.domain.model.BasketRepository;
import pl.softwareskill.sample.microservices.basket.domain.service.BasketService;
import pl.softwareskill.sample.microservices.basket.domain.service.NotFoundException;

import java.util.HashMap;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
class BasketController {

    private final BasketService basketService;
    private final BasketRepository basketRepository;
    private final BasketViewResponseFactory basketViewResponseFactory;

    @PostMapping("/basket")
    public ResponseEntity<?> createBasket() {
        log.info("Create basket");
        var basket = basketService.createBasket();
        return basketCreatedResponse(basket);
    }

    @DeleteMapping("/basket/{basketId}")
    public ResponseEntity<?> removeBasket(@PathVariable UUID basketId) {
        log.info("Remove basket {}", basketId);
        basketService.removeBasket(basketId);
        return basketRemovedResponse();
    }

    @GetMapping("/basket/{basketId}")
    public ResponseEntity<?> viewBasket(@PathVariable UUID basketId) {
        log.info("Get basket details {}", basketId);
        return basketRepository.findById(basketId)
                .map(basketViewResponseFactory::create)
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new NotFoundException("Basket not found"));
    }

    private ResponseEntity<?> basketCreatedResponse(Basket basket) {
        var body = new HashMap<>();
        body.put("basketId", basket.getBasketId());
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(body);
    }

    private ResponseEntity<Object> basketRemovedResponse() {
        return ResponseEntity.ok(null);
    }
}
