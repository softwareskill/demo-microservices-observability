package pl.softwareskill.sample.microservices.basket.domain.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.sleuth.SpanName;
import pl.softwareskill.sample.microservices.basket.domain.model.Basket;
import pl.softwareskill.sample.microservices.basket.domain.model.BasketRepository;
import pl.softwareskill.sample.microservices.basket.domain.model.Product;
import pl.softwareskill.sample.microservices.basket.domain.model.ProductRepository;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
public class BasketItemsService {

    private final BasketRepository basketRepository;
    private final ProductRepository productRepository;

    @SpanName("Insert Item")
    public Basket insertItem(UUID basketId, String productId, long quantity) {
        Basket basket = getBasket(basketId);
        Product product = getProduct(productId);

        basket.insert(product, quantity);
        log.info("Added product {} to basket: {}", productId, basketId);

        basketRepository.save(basket);
        return basket;
    }

    @SpanName("Remove Item")
    public Basket removeItem(UUID basketId, String productId) {
        Basket basket = getBasket(basketId);

        basket.remove(productId);
        log.info("Removed product {} from basket: {}", productId, basketId);

        basketRepository.save(basket);
        return basket;
    }

    private Product getProduct(String productId) {
        return productRepository.getProductById(productId)
                .orElseThrow(() -> new NotFoundException("Poduct not found."));
    }

    private Basket getBasket(UUID basketId) {
        return basketRepository.findById(basketId)
                .orElseThrow(() -> new NotFoundException("Basket not found."));
    }
}
