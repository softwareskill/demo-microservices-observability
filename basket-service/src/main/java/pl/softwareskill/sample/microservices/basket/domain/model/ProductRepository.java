package pl.softwareskill.sample.microservices.basket.domain.model;

import java.util.Optional;

public interface ProductRepository {

    Optional<Product> getProductById(String productId);
}
