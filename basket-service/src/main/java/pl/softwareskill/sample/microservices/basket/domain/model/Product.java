package pl.softwareskill.sample.microservices.basket.domain.model;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class Product {

    String productId;
    BigDecimal price;
}
