package pl.softwareskill.sample.microservices.basket.domain.service;

public class NotFoundException extends RuntimeException {

    public NotFoundException(String message) {
        super(message);
    }
}
