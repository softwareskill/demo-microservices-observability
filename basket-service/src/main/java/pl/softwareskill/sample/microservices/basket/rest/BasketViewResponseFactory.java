package pl.softwareskill.sample.microservices.basket.rest;

import lombok.RequiredArgsConstructor;
import pl.softwareskill.sample.microservices.basket.domain.model.Basket;
import pl.softwareskill.sample.microservices.basket.domain.model.BasketItem;
import pl.softwareskill.sample.microservices.basket.domain.model.Product;
import pl.softwareskill.sample.microservices.basket.domain.model.ProductRepository;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RequiredArgsConstructor
class BasketViewResponseFactory {

    final ProductRepository productRepository;

    public BasketViewResponse create(Basket basket) {
        return new BasketViewResponse(basket.getBasketId(), toItems(basket.getItems()));
    }

    private List<BasketViewResponse.Item> toItems(Collection<BasketItem> items) {
        return items.stream().map(this::toItem).collect(toList());
    }

    private BasketViewResponse.Item toItem(BasketItem item) {
        BigDecimal price = productRepository.getProductById(item.getProductId())
                .map(Product::getPrice)
                .orElse(BigDecimal.ZERO);

        return new BasketViewResponse.Item(item.getProductId(), item.getQuantity(), price);
    }
}
