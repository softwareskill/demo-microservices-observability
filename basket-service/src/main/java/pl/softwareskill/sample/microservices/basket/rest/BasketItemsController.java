package pl.softwareskill.sample.microservices.basket.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.softwareskill.sample.microservices.basket.domain.model.Basket;
import pl.softwareskill.sample.microservices.basket.domain.service.BasketItemsService;

import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
class BasketItemsController {

    private final BasketItemsService basketItemsService;
    private final BasketViewResponseFactory basketViewResponseFactory;

    @PostMapping("/basket/{basketId}/product")
    public ResponseEntity<?> insertProductToBasket(@PathVariable UUID basketId, @RequestBody BasketItemAddRequest request) {
        var basket = basketItemsService.insertItem(basketId, request.getProductId(), request.getQuantity());
        return basketViewResponse(basket);
    }

    @DeleteMapping("/basket/{basketId}/product/{productId}")
    public ResponseEntity<?> removeProductFromBasket(@PathVariable UUID basketId, @PathVariable String productId) {
        var basket = basketItemsService.removeItem(basketId, productId);
        return basketViewResponse(basket);
    }

    private ResponseEntity<Object> basketViewResponse(Basket basket) {
        return ResponseEntity.ok(basketViewResponseFactory.create(basket));
    }
}
