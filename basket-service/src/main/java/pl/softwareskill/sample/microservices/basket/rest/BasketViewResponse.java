package pl.softwareskill.sample.microservices.basket.rest;

import lombok.Value;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Value
class BasketViewResponse {

    UUID basketId;
    List<Item> items;

    @Value
    static class Item {
        String productId;
        long quantity;
        BigDecimal price;
    }
}
