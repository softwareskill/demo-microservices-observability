package pl.softwareskill.sample.microservices.basket.infra;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.softwareskill.sample.microservices.basket.domain.model.Product;
import pl.softwareskill.sample.microservices.basket.domain.model.ProductRepository;

import java.math.BigDecimal;
import java.util.Arrays;

@Configuration
class ProductRepositoryConfig {

    @Bean
    ProductRepository productRepository() {
        return new ProductRepositoryImpl(Arrays.asList(
                new Product("book1", BigDecimal.valueOf(39.50)),
                new Product("book2", BigDecimal.valueOf(35.00)),
                new Product("book3", BigDecimal.valueOf(19.90))
        ));
    }
}
