package pl.softwareskill.sample.microservices.basket.rest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.softwareskill.sample.microservices.basket.domain.model.ProductRepository;

@Configuration
class RestConfig {

    @Bean
    BasketViewResponseFactory basketViewResponseFactory(ProductRepository productRepository) {
        return new BasketViewResponseFactory(productRepository);
    }
}
