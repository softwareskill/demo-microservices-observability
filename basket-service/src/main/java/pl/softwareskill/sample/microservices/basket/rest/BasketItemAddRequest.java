package pl.softwareskill.sample.microservices.basket.rest;

import lombok.Data;

@Data
class BasketItemAddRequest {
    private String productId;
    private long quantity;
}
