package pl.softwareskill.sample.microservices.basket.domain.model;

import lombok.Value;

import java.io.Serializable;

@Value
public class BasketItem implements Serializable {

    String productId;
    long quantity;
}
