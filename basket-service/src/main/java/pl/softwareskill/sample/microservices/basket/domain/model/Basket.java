package pl.softwareskill.sample.microservices.basket.domain.model;

import lombok.Getter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.UUID;

public class Basket implements Serializable {

    @Getter
    private final UUID basketId;
    private final Collection<BasketItem> items;

    private Basket(UUID basketId) {
        this.basketId = basketId;
        this.items = new ArrayList<>();
    }

    public static Basket create() {
        return new Basket(UUID.randomUUID());
    }

    public void insert(Product product, long quantity) {
        var basketItem = items.stream()
                .filter(bi -> Objects.equals(product.getProductId(), bi.getProductId()))
                .findFirst();

        basketItem.ifPresent(items::remove);
        items.add(new BasketItem(product.getProductId(), quantity));
    }

    public void remove(String productId) {
        var basketItem = items.stream()
                .filter(bi -> Objects.equals(productId, bi.getProductId()))
                .findFirst();

        basketItem.ifPresent(items::remove);
    }

    public Collection<BasketItem> getItems() {
        return new ArrayList<>(items);
    }
}
