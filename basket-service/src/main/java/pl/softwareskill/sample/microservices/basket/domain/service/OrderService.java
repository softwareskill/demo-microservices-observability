package pl.softwareskill.sample.microservices.basket.domain.service;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
public class OrderService {

    private final WebClient webClient;
    private final Duration timeout;

    @NewSpan("Create Order")
    @SneakyThrows
    public UUID createOrder(String description) {
        Request request = Request.builder()
                .description(description)
                .build();

        return webClient
                .post()
                .uri("/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(request))
                .retrieve()
                .bodyToMono(Response.class)
                .map(Response::getOrderId)
                .timeout(timeout)
                .toFuture()
                .get(timeout.toMillis(), TimeUnit.MILLISECONDS);
    }

    @Data
    @Builder
    private static class Request {
        private String description;
    }

    @Data
    private static class Response {
        private UUID orderId;
    }
}
