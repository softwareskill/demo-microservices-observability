package pl.softwareskill.sample.microservices.basket.domain.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.softwareskill.sample.microservices.basket.domain.model.BasketRepository;
import pl.softwareskill.sample.microservices.basket.domain.model.ProductRepository;

@Configuration
class ServicesConfig {

    @Bean
    BasketService basketService(BasketRepository basketRepository) {
        return new BasketService(basketRepository);
    }

    @Bean
    BasketItemsService basketItemsService(BasketRepository basketRepository, ProductRepository productRepository) {
        return new BasketItemsService(basketRepository, productRepository);
    }
}
